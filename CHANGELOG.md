# Changelog

- 1.0.0:

  Initial

- 1.0.1:

  - Fixes:
    - Add long description and long description compatible with markdown
    - Add all files from example folder
